
The `script` utility dumps core when one attempts to log to a file in a 
non-existent directory.

```
~$ mkdir script-demo
~$ cd script-demo
~/script-demo$ script nosuchdir/file.txt
Script started, file is nosuchdir/file.txt
script: cannot open nosuchdir/file.txt: No such file or directory
                                                                 Script done, file is nosuchdir/file.txt
Segmentation fault (core dumped)

```




```
~$ which script
bsdutils: /usr/bin/script

~$ dpkg -S `which script`
Desired=Unknown/Install/Remove/Purge/Hold
| Status=Not/Inst/Conf-files/Unpacked/halF-conf/Half-inst/trig-aWait/Trig-pend
|/ Err?=(none)/Reinst-required (Status,Err: uppercase=bad)
||/ Name           Version             Architecture Description
+++-==============-===================-============-=================================
ii  bsdutils       1:2.27.1-6ubuntu3.4 amd64        basic utilities from 4.4BSD-Lite
```

One expects instead that the program exit with an error message:


```
~$ mkdir script-demo
~$ cd script-demo
~/script-demo$ script nosuchdir/file.txt
Script started, file is nosuchdir/file.txt
script: cannot open nosuchdir/file.txt: No such file or directory
```
