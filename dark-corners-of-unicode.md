
I came to the post [Dark corners of Unicode](https://eev.ee/blog/2015/09/12/dark-corners-of-unicode/)

via [a comment on lobste.rs](https://lobste.rs/s/hmo5jv/absolute_minimum_every_software#c_csqkde)

and noticed this typo or misspelling, the unnecessary *b* 

This:

"and the Roman numberal for ten"

should be:

" and the Roman numeral for ten"

I haven't finished reading it yet but it'll be easy enough to submit a
report given 

https://github.com/eevee/eev.ee/blob/master/content/2015-09-12-dark-corners-of-unicode.markdown


