
Starting from their main page
and following the 
[Resources](https://libraryfreedomproject.org/resources/)
link, one of the sections is for the 
[Mobile Privacy Crash Course](https://libraryfreedomproject.org/mobileprivacytoolkit/).

Following the link for that, then, shows a couple of stale resources.

The biggest one is for the broken link to the now defunct CyanogenMod
project. That should probably be replaced by a link to CyanogenMod's
successor, [LineageOS](https://lineageos.org/).

