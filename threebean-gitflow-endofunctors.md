

There's a link in

http://threebean.org/presentations/hfoss-lecture-2013-10-21/#/step-1

to the now-infamous tweet 

https://twitter.com/pornelski/status/316190292443267073

but which tweet is no longer available.

The Wayback Machine has it, though, at:

https://web.archive.org/web/20140328205442/https://twitter.com/pornelski/status/316190292443267073

the more involved blog post in this vein remains

http://tartley.com/?p=1267


