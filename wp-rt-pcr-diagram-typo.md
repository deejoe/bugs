
In the RT-PCR Wikipedia article:

https://en.wikipedia.org/wiki/Reverse_transcription_polymerase_chain_reaction

there's an instance of this diagram

https://upload.wikimedia.org/wikipedia/commons/e/e7/RT_PCR_Model.jpg

in which, at step `f` the wrong homophone is used. It has `complimentary`
but it should be `complementary`.

consider:

https://en.wiktionary.org/wiki/complimentary

https://en.wiktionary.org/wiki/complementary

Noted via

https://en.wikipedia.org/wiki/File_talk:RT_PCR_Model.jpg

