
Broken link from 

<http://250bpm.com/>

current leads to ribosome.ch, which is now a shop,
rather than to 

<http://sustrik.github.io/ribosome/>

which I infer based on authorship and style is the intended target.


The broken link was removed between:

https://web.archive.org/web/20170101000000*/http://250bpm.com/

and

https://web.archive.org/web/20171009164632/http://250bpm.com/


Typo in 

<http://250bpm.com/blog:93>

**dept** instead of the probably-intended **debt**.

