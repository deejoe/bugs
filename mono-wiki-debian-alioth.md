
The Debian wiki page for Mono[10] has a link to alioth, but alioth has been 
replaced by salsa[20] and so the link is broken.

[10]: https://wiki.debian.org/Mono
[20]: https://lists.debian.org/debian-devel-announce/2018/06/msg00001.html
