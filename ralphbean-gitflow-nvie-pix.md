

Links in [ralphbean](http://threebean.org/)'s [gitflow presentation](https://github.com/ralphbean/presentations/blob/master/gitflow.rst) to externally-hosted images are
broken, most likely because those images have moved. 

I've begun to track down where and what the links were, and what might be
the appropriate updated locations of those images, but this is so far
incomplete:

http://nvie.com/img/2009/12/Screen-shot-2009-12-24-at-11.32.03.png

http://nvie.com/img/git-model@2x.png

http://nvie.com/img/centr-decentr@2x.png

