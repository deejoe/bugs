
OK, backing out of earlier suggestion, which was based on a misunderstanding
of the document in question:

<https://linode.com/docs/tools-reference/custom-kernels-distros/install-a-custom-distribution-on-a-linode/#download-and-install-image>

The problem occurs if the reader loses track of the roles of the disk images
through the process.  In **Rescue Mode**, a pile of bits goes into a volume
known temporarily as ```/dev/sda```.  

Then, booting into the **Installer profile**, that pile of bits gets a
different name, ```/dev/sdb```.  Those bits are used to boot, but only
temporarily, during installation.  Unlike in **Rescue mode**, the name
```/dev/sda``` is applied to a different volume, a volume intended to receive the
installed system and eventually to become the boot volume.

Finally, in the **Boot profile**, that pile of bits, still under the name
```/dev/sda```, is used to boot the system.

At this point, I think I'll suggest changing the language so that the two
profiles do not have the same names as the two disk images. Use of the term
**boot** to indicate a profile or a disk image either one is a problem,
since one boots a system into each of three modes: Rescue, Installer, and
the final configuration, which I suggest here be called **Production** but
could be called **Final** or something else, so long as it does not collide
with the name used for the volume that is the ultimate destination of the
installation process.

So, instead of

  * **Installer** profile and **Installer** image
  * **Boot** profile and **Boot** image

we would have 

  * **Installation** profile, but **installer** image.
  * **Production** profile, but **target** image.

So, in Rescue Mode, one writes, eg, a GNU/Linux distribution's
download ISO file to the **installer** disk image.

When booted into the **Installation** profile, one boots from the **installer** image, but writes to the **target** image.

In the **Production** profile, one boots from the **target** image, and the **installer** image is dormant. 

The process optionally can stop here, or continue into the improvements
suggested by the rest of the document.

Again, the installation necessarily entails 3 boots--into **Rescue Mode**,
into the **Installer** profile, and into the **Boot** profile

It is easier to miss the switch in the role of the disk names and volume
roles through this process because this document visually draws out the two
profiles **Installer** and **Boot**.  In contrast, it somewhat buries the
implicit aspects of the existing volume from which the system is booted into
**Rescue Mode**.

