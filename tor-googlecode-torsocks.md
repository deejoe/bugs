

<https://www.torproject.org/docs/tor-doc-unix.html.en#using>

gives a link whose URL leads to a defunct Google Code project:

<https://code.google.com/p/torsocks/>

but which for now gives a newer URL back at the torproject.org domain:

<https://gitweb.torproject.org/torsocks.git/>


