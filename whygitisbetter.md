
The site

  <http://whygitisbetterthanx.com/>

is offline.

The Wayback Machine history leads me to conclude that it was abandoned,
squatted on, and then abandoned by those that squatted it.

Reference is made to the site in

  <https://commons.wikimedia.org/wiki/File:Git_Basics_no._4_-_Quick_Wins_with_Git.webm>

at around the 4 minute mark.

The source of the page is availabe under an MIT Expat style license at:

  <https://github.com/schacon/whygitisbetter>

