
Server answers to www via https, but cert supplied is for non-www site.

Source of link:

<https://allthingsopen.org/speakers/elizabeth-joseph/>

Leads to certificate mismatch error

> www.opensourceinfra.org uses an invalid security certificate. The certificate is only valid for opensourceinfra.org Error code: SSL_ERROR_BAD_CERT_DOMAIN

when arriving at

<https://www.opensourceinfra.org/>

The site

<https://opensourceinfra.org/>

is just fine.

