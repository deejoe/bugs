# Tue Mar 31 08:30:43 EDT 2020

On Ubuntu 18.04 amd64:


```
~/forth$ strace yforth
execve("/usr/bin/yforth", ["yforth"], 0x7ffdcf8bdfe0 /* 59 vars */) = 0
brk(NULL)                               = 0x56103e000000
access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
access("/etc/ld.so.preload", R_OK)      = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=143030, ...}) = 0
mmap(NULL, 143030, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f9e8f684000
close(3)                                = 0
access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/lib/x86_64-linux-gnu/libm.so.6", O_RDONLY|O_CLOEXEC) = 3
read(3, "\177ELF\2\1\1\3\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\200\272\0\0\0\0\0\0"..., 832) = 832
fstat(3, {st_mode=S_IFREG|0644, st_size=1700792, ...}) = 0
mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f9e8f682000
mmap(NULL, 3789144, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f9e8f0e2000
mprotect(0x7f9e8f27f000, 2093056, PROT_NONE) = 0
mmap(0x7f9e8f47e000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x19c000) = 0x7f9e8f47e000
close(3)                                = 0
access("/etc/ld.so.nohwcap", F_OK)      = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/lib/x86_64-linux-gnu/libc.so.6", O_RDONLY|O_CLOEXEC) = 3
read(3, "\177ELF\2\1\1\3\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0\260\34\2\0\0\0\0\0"..., 832) = 832
fstat(3, {st_mode=S_IFREG|0755, st_size=2030544, ...}) = 0
mmap(NULL, 4131552, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f9e8ecf1000
mprotect(0x7f9e8eed8000, 2097152, PROT_NONE) = 0
mmap(0x7f9e8f0d8000, 24576, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x1e7000) = 0x7f9e8f0d8000
mmap(0x7f9e8f0de000, 15072, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f9e8f0de000
close(3)                                = 0
mmap(NULL, 12288, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f9e8f67f000
arch_prctl(ARCH_SET_FS, 0x7f9e8f67f740) = 0
mprotect(0x7f9e8f0d8000, 16384, PROT_READ) = 0
mprotect(0x7f9e8f47e000, 4096, PROT_READ) = 0
mprotect(0x56103c3f7000, 12288, PROT_READ) = 0
mprotect(0x7f9e8f6a7000, 4096, PROT_READ) = 0
munmap(0x7f9e8f684000, 143030)          = 0
brk(NULL)                               = 0x56103e000000
brk(0x56103e021000)                     = 0x56103e021000
openat(AT_FDCWD, "yforth", O_RDONLY)    = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "YFORTH.BLK", O_RDWR)  = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "YFORTH.BLK", O_RDWR)  = -1 ENOENT (No such file or directory)
rt_sigaction(SIGFPE, {sa_handler=0x56103c1f10b0, sa_mask=[FPE], sa_flags=SA_RESTORER|SA_RESTART, sa_restorer=0x7f9e8ed2ff20}, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0
rt_sigaction(SIGSEGV, {sa_handler=0x56103c1f10f0, sa_mask=[SEGV], sa_flags=SA_RESTORER|SA_RESTART, sa_restorer=0x7f9e8ed2ff20}, {sa_handler=SIG_DFL, sa_mask=[], sa_flags=0}, 8) = 0
fstat(1, {st_mode=S_IFCHR|0620, st_rdev=makedev(136, 19), ...}) = 0
write(1, "yForth? v0.2  Copyright (C) 2012"..., 209yForth? v0.2  Copyright (C) 2012  Luca Padovani
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions; see LICENSE for details.
) = 209
--- SIGSEGV {si_signo=SIGSEGV, si_code=SEGV_MAPERR, si_addr=0x3e000dd8} ---
rt_sigaction(SIGSEGV, {sa_handler=0x56103c1f10f0, sa_mask=[SEGV], sa_flags=SA_RESTORER|SA_RESTART, sa_restorer=0x7f9e8ed2ff20}, {sa_handler=0x56103c1f10f0, sa_mask=[SEGV], sa_flags=SA_RESTORER|SA_RESTART, sa_restorer=0x7f9e8ed2ff20}, 8) = 0
write(1, "error(17): segmentation fault.\n", 31error(17): segmentation fault.
) = 31
--- SIGSEGV {si_signo=SIGSEGV, si_code=SI_KERNEL, si_addr=NULL} ---
+++ killed by SIGSEGV (core dumped) +++
Segmentation fault (core dumped)
```

