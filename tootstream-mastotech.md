
I just popped into the tmux window in which I have running a tootstream
instance working against my @ritjoe@mastotech account, only to see that it stopped
streaming a couple of days ago:

```
  Now Playing in the Toot-Lab @nowplaying@toot-lab.reclaim.technology 2018-03-10 15:36:01 (21 seconds ago)
  ♺:1 ♥:0 id:76 vis:🌎
  🎶 #nowplaying #np #tootlabradio #fediplay 🎶

  The Brand New Heavies - "You Are The Universe"

  https://www.youtube.com/watch?v=BeVutyFHYKk

  🎵 https://libre.fm/artist/The+Brand+New+Heavies/album/Shelter/track/You+Are+
  The+Universe 🎵

Something went wrong: ('Connection broken: IncompleteRead(0 bytes read)', IncompleteRead(0 bytes read))
[@ritjoe (mastotech)]:
```

My use here of tootstream with this account serves at least two purposes for
me:

  * It's a role-specific account in which I *try* to keep separate at least some of my RIT-related work.
  * It's an account separate from my @deejoe@mstdn.io account, in which to some degree I can compare and contrast the server-specific interactions of that account with tootstream, most obviously to me the severe bugs I've noticed in the interaction between that server and that client: https://github.com/magicalraccoon/tootstream/issues/163

The latter role, of contrasting server-specific behaviors with the same client, seems relevant to that latter bug.

There's a certain level of *not it* happening between the admin of that instance (@angristan) and folks helping maintain that client (@craigmaloney), perhaps understandably so, but still.

In any case, I think I'll open a new issue against tootstream, but since I
haven't yet discovered what the relevant mentions are for mastotech, rather
than fall prey to the yak shaving predators that swallow reports on their
way at least to being reported successfully, I'm going to stash this in my own bugjar
for now and will pop it back out and polish it back up as time allows and as
additional circumstance demands.

