
In the blog post [Chasing Quick Fixes To Sustainability][10] there is an
italicized aside noting that the article has been cross-posted to the
Software Freedom Conservancy web site, but the link given points back to
itself, the same article on the ebb.org blog. Clicking it is effectively a
null op.

There is in fact an item on the sfconservancy.org web [under the same
title][20] but I see no link from it back to the ebb.org version (which is
OK, just noting there is no reciprocal bug).

[10]: http://ebb.org/bkuhn/blog/2019/05/23/github-sponsors.html
[20]: https://sfconservancy.org/blog/2019/may/23/github-sponsors/
