
The emacs wikii page for diary mode:

<https://www.emacswiki.org/emacs/DiaryMode>

simply has an instance of misspelling: 

> wether

which should be corrected to read as *whether*.

