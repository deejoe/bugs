
David A. Wheeler in his **How to Evaluate Open Source Software / Free Software (OSS/FS) Programs**
as revised August 5, 2011 an as found here:

<https://www.dwheeler.com/oss_fs_eval.html> 
[Wayback Machine version](https://web.archive.org/web/20170824001553/https://www.dwheeler.com/oss_fs_eval.html)

Includes two references to **The IDA Open Source Migration Guidelines** via
a now-broken link, http://europa.eu.int/ISPO/ida/jsps/index.jsp?fuseAction=showDocument&parent=news&documentID=1647

The Internet Archive's Wayback Machine holds at least this archived copy of
that page:

<https://web.archive.org/web/20031202191002/http://europa.eu.int/ISPO/ida/jsps/index.jsp?fuseAction=showDocument&parent=news&documentID=1647>

and of the PDF to which that page then points (this is for the English
version, there is at least a link also to a version in French):

<https://web.archive.org/web/20031121150329/http://europa.eu.int:80/ISPO/ida/export/files/en/1618.pdf>

(Wheeler's article contains a great many links, many of which I've not yet checked, but which checking might benefit from a spot of automation.)

