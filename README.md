
# random bug reports

_In FOSS America, you don't find bugs, bugs find you._

These are bugs that I stumble across, as one does, in the course of doing
other things.  As such, they are always a surprise and, usually, an
annoyance.  But, my stack can only get so deep, so to drop everything and
try to document and report the bug to the right people in the right way is,
to be honest, too much of a hassle for a lot of bugs that are more easily
worked around or accomodated.  Other times, there's nothing I'd like better
to do than to go haring off after the bug, but I don't feel I can spend the
time, because I already *had* a task I was supposed to be doing.

A lot of the bugs I meet which I feel I can address are pretty simple:
Broken links or misspellings or grammar errors or other typos, or they're
wishlist items.  So, in a lot of ways, these are harder bugs to deal with
right off the bat because, unlike code, documentation and web sites often do
not have source repositories or bug trackers set up, or, at least, that are
as easy to find and navigate.

The first step of reporting a bug is to document it. Write down what
happens.  (Or, if this is how you work, or more appropriate to the task in
some other way, to screenshot it, or to record video or whatever.  Get a
record, at any rate.)

So, my first impulse is to reach for my text editor. That's where this
repository comes in.  I'm already used to checking in text changes and
pushing them up to a public place, why not do this for my bug write ups?

This way, if the bug reporting mechanism is the "send an email" and I've
already pushed a short write-up of the bug to this repository, I can just
send a link!

If there is a more structured bug-reporting mechanism, at least I'll have
some of the details in hand wherever and whenever I am procrastinating hard
enough that sending the report in seems to be an attractive way to spend the
time.

So, we'll see how this goes. Maybe if it works out, and I end up reporting
these bugs and they get fixed, or they get fixed some other way, I'll be
able to `git mv some-bug fixed/`

