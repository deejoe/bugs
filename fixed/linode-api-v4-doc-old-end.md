
The URL previously noted here is now 404.

Navigating down from developers.linode.com I find

https://developers.linode.com/api/docs/v4#section/Filtering-and-Sorting

in which the example is completely different, and presumably, corrected.

Pending testing, I'm going to shift this over into the 'fixed' folder.

The link to the API changelog noted below is also 404, and currently seems 
to be

https://developers.linode.com/changelog/api/

The entry from the date below remains.

# deprecated endpoint in API docs

In, eg,

https://developers.linode.com/v4/filtering

but also elsewhere in the API documentation, the 'distributions' endpoint is
used as an example, yet that endpoint was deprecated, as per the *Breaking*
section of the **2018-02-26** entry in

https://developers.linode.com/v4/changelogs

All these examples should be re-worked using a current endpoint.

(I tried to sort out if, and if so where, in https://github.com/linode this
documentation sits, but with no luck just yet).

