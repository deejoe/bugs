
Starting from their main page
and following the 
[Resources](https://libraryfreedomproject.org/resources/)
link, one of the sections is for the 
[Mobile Privacy Crash Course](https://libraryfreedomproject.org/mobileprivacytoolkit/).

Following the link for that, then, shows a couple of stale resources.

A more minor omission is in listing only [OwnCloud](https://owncloud.com/).

For more complete coverage in this area, [NextCloud](https://nextcloud.com/) should also be
listed.

