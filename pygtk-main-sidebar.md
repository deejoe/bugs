

In the main PyGTK page:

<http://www.pygtk.org/index.html>

in the right-hand sidebar under the headings:

> Documentation
> Wiki

There is a link to a wiki page that does not exist:

<https://wiki.gnome.org/PyGTK>

> This page does not exist yet. You can create a new empty page, or use one of
> the page templates."


However, on the same wiki this page exists and is the presumbable original
target of the link from the sidebar, but was perhaps moved in a
reorganization:

<https://wiki.gnome.org/Projects/PyGTK>


