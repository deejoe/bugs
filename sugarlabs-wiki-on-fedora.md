
The Sugar Labs wiki page for Fedora

<http://wiki.sugarlabs.org/go/Fedora>

has the following links for Sugar packages, but they return with a **No repositories found**
error:

  * <http://pkgs.fedoraproject.org/cgit/sugar.git/>
  * <http://pkgs.fedoraproject.org/cgit/sugar-artwork.git/>
  * <http://pkgs.fedoraproject.org/cgit/sugar-toolkit-gtk3.git/>
  * <http://pkgs.fedoraproject.org/cgit/sugar-datastore.git/>

These links currently (2017-12-18) work:

  * <https://src.fedoraproject.org/rpms/sugar>
  * <https://src.fedoraproject.org/rpms/sugar-artwork>
  * <https://src.fedoraproject.org/rpms/sugar-toolkit-gtk3>
  * <https://src.fedoraproject.org/rpms/sugar-datastore>

