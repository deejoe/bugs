
# deprecated endpoint in API docs

In, eg,

https://developers.linode.com/v4/filtering

but also elsewhere in the API documentation, the 'distributions' endpoint is
used as an example, yet that endpoint was deprecated, as per the *Breaking*
section of the **2018-02-26** entry in

https://developers.linode.com/v4/changelogs

All these examples should be re-worked using a current endpoint.

(I tried to sort out if, and if so where, in https://github.com/linode this
documentation sits, but with no luck just yet).

