

https://support.mozilla.org/en-US/kb/tabs-organize-websites-single-window#w_tab-tips

> To bookmark a set of tabs all at once use the keyboard shortcut Ctrl + Shift + D. 

This one appears to be a documentation bug, since by all accounts the option
is deliberately gone from the top-tier menus, having been relegated to the
tabs contextual menu.

