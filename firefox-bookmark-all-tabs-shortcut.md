
So far as I can tell, this functionality is still missing from current 
releases of Firefox. Specifically, there remains no keyboard shortcut for 
bookmarking a set of tabs.

The page in question, however, has been updated to reflect the rather more 
complex, mouse-intensive way to click through to a similar result:

https://support.mozilla.org/en-US/kb/tabs-organize-websites-single-window#w_tab-tips


The previous behavior described here

> To bookmark a set of tabs all at once use the keyboard shortcut Ctrl + Shift + D. 

can be seen in this archived version:

https://web.archive.org/web/20170925080521/https://support.mozilla.org/en-US/kb/tabs-organize-websites-single-window

So far as I can tell, there is no key combination for opening the tab 
contextual menu, which leaves this functionality inaccessible using strictly 
the keyboard. It might be possible to use some accessibility tools here, I 
don't know, but in the meantime I'm continuing to see this as a regression 
(even if upstream sees this as wontfix).

 

